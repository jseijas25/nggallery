import { Image } from './../../models/image';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-image',
  templateUrl: './image.component.html',
  styles: [ `img {
  -webkit-box-shadow: 0px 1px 6px 1px rgba(0,0,0,0,75);
  -moz-boz-shadow: 0px 1px 6px 1px rgba(0,0,0,0,75);
  box-shadow:  0px 1px 6px 1px rgba(0,0,0,0,75);
  margin-bottom: 20px;
}

img:hover {
  filter: gray;
  -webkit-filter: grayscale(1);
} `]
})
export class ImageComponent implements OnInit {
  @Input() image:Image;
  constructor() { }

  ngOnInit() {
  }

}
