import { Image } from './../../models/image';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-image-list',
  templateUrl: './image-list.component.html',
  styles: []
})
export class ImageListComponent implements OnInit {
  images: Image[] = [

  new Image ('1','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa1-1.jpg'),
  new Image ('2','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa2-1.jpg'),
  new Image ('3','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa3-1.jpg'),
  new Image ('4','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa4-1.jpg'),
  new Image ('5','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa5-1.jpg'),
  new Image ('6','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa6-1.jpg'),
  new Image ('7','Primera Imagen','Descripcion Primera Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa7-1.jpg'),

  ];
  constructor() { }

  ngOnInit() {
  }

}
